import React from "react";
import "./App.scss";
import { QueryClient, QueryClientProvider } from "react-query";
import Dashboard from "./components/dashboard";

const queryClient = new QueryClient();

function App() {
  const API_KEY = "b3d042dcd47aa02f077280d7358a0082";

  return (
    <div className="App">
      <QueryClientProvider client={queryClient}>
        <Dashboard />
      </QueryClientProvider>
    </div>
  );
}

export default App;
