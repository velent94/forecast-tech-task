import React from "react";
import {
  TForecastMainData,
  TForecastWeatherIconData,
  TForecastWindData,
} from "..";
import DashboardItem from "./DashboardItem";

export type TDashboardItemPops = {
  mainForecastData: TForecastMainData | undefined;
  windForecastData: TForecastWindData | undefined;
  iconData: TForecastWeatherIconData | undefined;
};

export default DashboardItem;
