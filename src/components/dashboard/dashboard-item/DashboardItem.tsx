import React from "react";
import { TDashboardItemPops } from ".";
import { getDirectionFromDegrees } from "../../../helpers";
import "./styles.scss";
const DashboardItem = ({
  mainForecastData,
  windForecastData,
  iconData,
}: TDashboardItemPops) => {
  return (
    <>
      {mainForecastData && windForecastData && (
        <div className="dashboardItemContainer">
          <div className="dashboardItem dashboardItem_temperature">
            <h1 className="dashboardItem_header">Temperature</h1>
            <img
              src={`http://openweathermap.org/img/wn/${iconData?.icon}@2x.png`}
              alt="weather icon"
            ></img>
            <ul className="dashboardItem_list">
              <li>
                {" "}
                <img
                  src="https://img.icons8.com/external-outlines-amoghdesign/344/external-forecast-agriculture-gardening-outlines-amoghdesign.png"
                  alt="wind icon"
                  width="25px"
                  height="25px"
                />
                {`Minimum temperature: ${Math.round(
                  mainForecastData.temp_min
                )}°C`}
              </li>
              <li>
                {" "}
                <img
                  src="https://img.icons8.com/external-outlines-amoghdesign/344/external-forecast-agriculture-gardening-outlines-amoghdesign.png"
                  alt="wind icon"
                  width="25px"
                  height="25px"
                />
                {`Maximum temperature: ${Math.round(
                  mainForecastData.temp_max
                )}°C`}
              </li>
            </ul>
          </div>
          <div className="dashboardItem dashboardItem_wind">
            <h1 className="dashboardItem_header">Wind</h1>
            <img
              src={`http://openweathermap.org/img/wn/${iconData?.icon}@2x.png`}
              alt="weather icon"
            ></img>
            <ul className="dashboardItem_list">
              <li>
                {" "}
                <img
                  src="https://img.icons8.com/external-glyphons-amoghdesign/344/external-forecast-marine-and-nautical-glyphons-amoghdesign.png"
                  alt="wind icon"
                  width="25px"
                  height="25px"
                />
                {`Wind speed: ${windForecastData.speed}m/s`}
              </li>
              <li>
                {" "}
                <img
                  src="https://img.icons8.com/ios/344/wind-rose.png"
                  alt="wind icon"
                  width="25px"
                  height="25px"
                />
                {`Wind direction: ${getDirectionFromDegrees(
                  windForecastData.deg
                )}`}
              </li>
            </ul>
          </div>

          <div className="dashboardItem dashboardItem_humidity">
            <h1 className="dashboardItem_header">Humidity</h1>
            <img
              src={`http://openweathermap.org/img/wn/${iconData?.icon}@2x.png`}
              alt="weather icon"
            ></img>
            <ul className="dashboardItem_list">
              <li>
                {" "}
                <img
                  src="https://img.icons8.com/ios/344/drop-of-blood--v1.png"
                  alt="wind icon"
                  width="25px"
                  height="25px"
                />
                {`Humidity: ${mainForecastData.humidity}%`}
              </li>
            </ul>
          </div>
          <div className="dashboardItem dashboardItem_pressure">
            <h1 className="dashboardItem_header">Pressure</h1>
            <img
              src={`http://openweathermap.org/img/wn/${iconData?.icon}@2x.png`}
              alt="weather icon"
            ></img>
            <ul className="dashboardItem_list">
              <li>
                {" "}
                <img
                  src="https://img.icons8.com/ios/344/barometer-gauge.png"
                  alt="wind icon"
                  width="25px"
                  height="25px"
                />
                {`Pressure: ${mainForecastData.pressure}hPa`}
              </li>
            </ul>
          </div>
        </div>
      )}
    </>
  );
};

export default DashboardItem;
