import Dashboard from "./Dashboard";

// Countries data types

export type TCapitalInfo = {
  latlng: Array<string>;
};
export type TCar = {
  car: string;
  signs: Array<string>;
};

export type TCoatOfArms = {
  png: string;
  svg: string;
};

export type TCurrencies = {
  EUR: {
    name: string;
    symbol: string;
  };
};

export type TDemonyms = {
  eng: {
    f: string;
    m: string;
  };
  fra: {
    f: string;
    m: string;
  };
};

export type TFlags = {
  png: string;
  svg: string;
};

export type TIdd = {
  root: string;
  suffixes: Array<string>;
};

export type TLanguages = {
  [key: string]: string;
};

export type TMaps = {
  googleMaps: string;
  openStreetMaps: string;
};

export type TName = {
  common: string;
  nativeName: {
    [key: string]: {
      common: string;
      official: string;
    };
  };
  official: string;
};

export type TTranslations = {
  ara: {
    common: string;
    official: string;
  };
  cym: {
    common: string;
    official: string;
  };
  deu: {
    common: string;
    official: string;
  };
  est: {
    common: string;
    official: string;
  };
  fin: {
    common: string;
    official: string;
  };
  fra: {
    common: string;
    official: string;
  };
  hrv: {
    common: string;
    official: string;
  };
  hun: {
    common: string;
    official: string;
  };
  ita: {
    common: string;
    official: string;
  };
  jpn: {
    common: string;
    official: string;
  };
  kor: {
    common: string;
    official: string;
  };
  nld: {
    common: string;
    official: string;
  };
  per: {
    common: string;
    official: string;
  };
  pol: {
    common: string;
    official: string;
  };
  por: {
    common: string;
    official: string;
  };
  rus: {
    common: string;
    official: string;
  };
  slk: {
    common: string;
    official: string;
  };
  spa: {
    common: string;
    official: string;
  };
  swe: {
    common: string;
    official: string;
  };
  urd: {
    common: string;
    official: string;
  };
  zho: {
    common: string;
    official: string;
  };
};

export type TFetchCountryResponse = {
  altSpellings: Array<string>;
  area: number;
  capital: Array<string>;
  capitalInfo: TCapitalInfo;
  car: TCar;
  cca2: string;
  cca3: string;
  ccn3: string;
  coatOfArms: TCoatOfArms;
  continents: Array<string>;
  currencies: TCurrencies;
  demonyms: TDemonyms;
  flag: string;
  flags: TFlags;
  idd: TIdd;
  independent: boolean;
  landlocked: boolean;
  languages: TLanguages;
  latlng: Array<string>;
  maps: TMaps;
  name: TName;
  population: number;
  region: string;
  startOfWeek: string;
  status: string;
  subregion: string;
  timezones: Array<string>;
  tld: Array<string>;
  translations: TTranslations;
  unMember: boolean;
};

export type TFetchCountriesResponse = Array<TFetchCountryResponse> | undefined;

export type TSelectedCountryCoordinates = {
  lat: string | undefined;
  lon: string | undefined;
};

// Forecast data types

export type TForecastWeatherIconData = {
  id: number;
  main: string;
  description: string;
  icon: string;
};

export type TForecastClouds = {
  all: number;
};

export type TForecastCoordinates = {
  lat: number;
  lon: number;
};

export type TForecastMainData = {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
  sea_level: number;
  grnd_level: number;
};

export type TForecastSysData = {
  type: number;
  id: number;
  country: string;
  sunrise: number;
  sunset: number;
};

export type TForecastWindData = {
  speed: number;
  deg: number;
  gust: number;
};

export type TForecastResponse = {
  base: string;
  clouds: TForecastClouds;
  cod: number;
  coord: TForecastCoordinates;
  dt: number;
  id: number;
  main: TForecastMainData;
  name: string;
  sys: TForecastSysData;
  timezone: number;
  visibility: number;
  weather: Array<TForecastWeatherIconData>;
  wind: TForecastWindData;
};

export default Dashboard;
