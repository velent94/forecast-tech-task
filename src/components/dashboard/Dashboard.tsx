import React, { useState } from "react";
import { useQuery } from "react-query";
import {
  TFetchCountriesResponse,
  TForecastResponse,
  TSelectedCountryCoordinates,
} from ".";
import {
  APP_ID,
  fetchCountries,
  getCoordinates,
  getSpinnerBeforeForecast,
} from "../../helpers";
import CustomDropdown from "../custom-dropdown";
import DashboardItem from "./dashboard-item";
import "./styles.scss";

const Dashboard: React.FC = () => {
  const { data: countriesData } = useQuery<TFetchCountriesResponse>(
    "countries",
    fetchCountries,
    {
      refetchOnWindowFocus: false,
    }
  );
  const [selectedCountryCoordinates, setSelectedCountryCoordinates] =
    useState<TSelectedCountryCoordinates | null>(null);

  const onChangeHandler = (
    ev: React.ChangeEvent<HTMLSelectElement> | undefined
  ) => {
    const target = ev?.target as HTMLSelectElement;
    const coordinates = getCoordinates(target.value);
    setSelectedCountryCoordinates(coordinates);
  };

  const { lat, lon } = { ...selectedCountryCoordinates };

  const { status: forecastStatus, data: forecastData } =
    useQuery<TForecastResponse>(
      ["forecast", { lat, lon }],
      async () => {
        const resp = await fetch(
          `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&units=metric&appid=${APP_ID}`
        );
        console.log("fetching forecast");
        return resp.json();
      },
      { enabled: !!selectedCountryCoordinates, refetchOnWindowFocus: false }
    );
  console.log(forecastStatus, forecastData);
  const spinner = getSpinnerBeforeForecast(forecastStatus);

  return (
    <div className="dashboard-container">
      <h1>{"WELCOME TO AWESOME FORECAST"}</h1>
      <CustomDropdown options={countriesData} changeHandler={onChangeHandler} />
      {forecastStatus !== "success" && spinner}
      <DashboardItem
        windForecastData={forecastData?.wind}
        mainForecastData={forecastData?.main}
        iconData={forecastData?.weather[0]}
      />
    </div>
  );
};

export default Dashboard;
