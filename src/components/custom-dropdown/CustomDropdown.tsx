import React from "react";
import { TDropdownProps } from ".";
import "./styles.scss";

export const CustomDropdown: React.FC<TDropdownProps> = ({
  changeHandler,
  options,
  ...selectAttributes
}: TDropdownProps) => {
  return (
    <div className="dropdownContainer">
      <select
        className="dropdown"
        {...selectAttributes}
        onChange={changeHandler}
      >
        <option>{"Choose county"}</option>
        {options?.map((option) => (
          <option
            key={JSON.stringify(option.latlng[0]).concat(option.latlng[1])}
            value={`${option.latlng[0]} ${option.latlng[1]}`}
          >
            {option.name.official}
          </option>
        ))}
      </select>
    </div>
  );
};

export default CustomDropdown;
