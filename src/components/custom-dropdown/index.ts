import { TFetchCountriesResponse } from "../dashboard";
import CustomDropdown from "./CustomDropdown";

export interface TDropdownProps
  extends React.HTMLAttributes<HTMLSelectElement> {
  changeHandler: (ev: React.ChangeEvent<HTMLSelectElement> | undefined) => void;
  options: TFetchCountriesResponse;
}

export default CustomDropdown;
