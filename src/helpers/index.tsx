export const APP_ID = "b3d042dcd47aa02f077280d7358a0082";

export const fetchCountries = async () => {
  const resp = await fetch(`https://restcountries.com/v3.1/all`);
  console.log("fetching data");
  return resp.json();
};

export const getCoordinates = (coordinates: string | null) => {
  const splittedCoordinates = coordinates?.split(" ");
  return {
    lat: splittedCoordinates && splittedCoordinates[0],
    lon: splittedCoordinates && splittedCoordinates[1],
  };
};

export const getSpinnerBeforeForecast = (forecastStatus: string) => {
  switch (forecastStatus) {
    default:
      return <h1>Choose your country and receive current weather forecast</h1>;
    case "loading":
      return <h1>Loading...</h1>;
    case "error":
      return <h1>Something goes wrong</h1>;
  }
};

export const getDirectionFromDegrees = (degrees: number) => {
  const directionIndex = Math.round(degrees / 22.5 + 0.5);
  const directionsArray = [
    "N",
    "NNE",
    "NE",
    "ENE",
    "E",
    "ESE",
    "SE",
    "SSE",
    "S",
    "SSW",
    "SW",
    "WSW",
    "W",
    "WNW",
    "NW",
    "NNW",
  ];
  return directionsArray[directionIndex % 16];
};
